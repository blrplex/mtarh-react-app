const path = require('path')

module.exports = {
  stories: [__dirname, '../src/stories/**/*.stories.(js|jsx)'],
  addons: ['@storybook/addon-essentials'],
  webpackFinal: (config) => {
    return {
      ...config,
      resolve: {
        ...config.resolve,
        alias: {
          ...config.resolve.alias,
          '@': path.resolve(__dirname, '../src'),
        },
      },
    }
  },
}

# Maksim Tarhunakau lightweight React-Redux-Saga starter kit
It's a lightweight starter based on React-Redux-Saga kit. It includes Ctyled-Components, Jest, @testing-library/react, Storybook and splitted configurations for development and production.

### Installing
```sh
yarn
```
Use this command for downloading and installing all the dependencies. Please be sure that you use modern version of Node.js (at the moment it ready to deal with version 12).

### Development
```sh
yarn start
yarn start:prod
```
Runs application in development mode. You can customize configuration in 'config/webpack.dev.js'. Application will start on port 8080 but you can override it in ".env" file that initially is not hidden from repository in .gitignore file. The "prod" modifier allows to run application in production mode and test it additionally.

### Production
```sh
yarn build
```
Prepares a production build. It generates a new product version in package.json file. You can override configuration for this mode in 'config/webpack.production.js'.

### Storybook
```sh
yarn storybook
yarn build-storybook
```
Opens a storybook with all registered stories. By default it works on port 6006.

### Test
```sh
yarn test
```
Runs tests with Jest. It runs all the '*.test.js' files in all the project.

// const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const TerserJSPlugin = require('terser-webpack-plugin')

const common = require('./webpack.common')

module.exports = {
  ...common,
  devtool: false,
  mode: 'production',
  plugins: [
    ...common.plugins,
    // new UglifyJsPlugin({
    //   test: /\.js(\?.*)?$/i,
    //   extractComments: true,
    // }),
  ],
  optimization: {
    ...common.optimization,
    minimize: true,
    minimizer: [
      new TerserJSPlugin({
        test: /\.js(\?.*)?$/i,
      }),
    ],
  },
  performance: {
    maxEntrypointSize: 220000,
    maxAssetSize: 220000,
  },
}

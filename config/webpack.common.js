const webpack = require('webpack')
const path = require('path')

const WebpackAutoInject = require('webpack-auto-inject-version')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')

const analyzeBundles = process.argv.some(arg => arg === '--analyze-bundle')

const config = {
  mode: process.env.NODE_ENV || 'development',
  entry: {
    app: path.resolve(__dirname, '../src'),
  },
  output: {
    filename: '[name].[hash].js',
    path: path.resolve(__dirname, '../dist'),
  },
  module: {
    rules: [
      {
        test: /\.s[ac]ss$/i,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        loader: 'file-loader',
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.less'],
    modules: [path.resolve(__dirname, '../node_modules')],
    alias: {
      '@': path.resolve(__dirname, '../src'),
    },
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      title: 'MTarh Boilerplate',
      filename: 'index.html',
      inject: 'body',
    }),
    new webpack.HashedModuleIdsPlugin(),
    new WebpackAutoInject({
      components: {
        SHORT: 'CUSTOM',
        SILENT: false,
        PACKAGE_JSON_PATH: path.resolve(__dirname, '../package.json'),
        PACKAGE_JSON_INDENT: 2,
        components: {
          AutoIncreaseVersion: true,
          InjectAsComment: true,
          InjectByTag: true,
        },
        componentsOptions: {
          AutoIncreaseVersion: {
            runInWatchMode: false,
          },
          InjectAsComment: {
            tag: 'Build version: {version} - {date}.',
          },
        },
      },
    }),
    new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en/),
  ],
  optimization: {
    // splitChunks: {
    //   chunks: 'async',
    //   maxSize: 220000,
    //   minChunks: 1,
    //   maxAsyncRequests: 5,
    //   maxInitialRequests: 3,
    //   automaticNameDelimiter: '~',
    //   name: true,
    // },
  },
}

if (analyzeBundles) {
  config.plugins.push(new BundleAnalyzerPlugin())
}

module.exports = config

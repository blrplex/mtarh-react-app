const path = require('path')
const webpack = require('webpack')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
require('dotenv').config()

const common = require('./webpack.common')

module.exports = {
  ...common,
  devtool: 'inline-source-map',
  mode: 'development',
  devServer: {
    contentBase: path.resolve(__dirname, '../dist'),
    port: process.env.PORT || 8080,
    hot: true,
    headers: {
      'Cache-Control': 'max-age=31557600',
    },
  },
  plugins: [
    ...common.plugins,
    // new BundleAnalyzerPlugin(),  // uncomment for analyzing bundle
    new webpack.HotModuleReplacementPlugin(),
  ],
}

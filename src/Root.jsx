import React from 'react'
import { Provider } from 'react-redux'
import { ThemeProvider } from 'styled-components'

import { configureStore } from '@/store'
import Router from '@/Router'
import GlobalStyle from '@/theme/GlobalStyle'

import theme from '@/theme/config'

export const store = configureStore()

export default () => (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <Router />
    </ThemeProvider>
  </Provider>
)

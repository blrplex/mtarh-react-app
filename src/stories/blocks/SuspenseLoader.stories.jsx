import React from 'react'

import { storiesOf } from '@storybook/react'

import SuspenseFallback from '@/components/system/SuspenseFallback'

storiesOf('Shared Components', module)
  .add('SuspenseFallback', () => (
    <SuspenseFallback />
  ))

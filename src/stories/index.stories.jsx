import React from 'react'
import { storiesOf } from '@storybook/react'

import Greeter from '@/components/blocks/Greeter'

storiesOf('Shared Components', module)
  .add('Greeter', () => (
    <Greeter />
  ))

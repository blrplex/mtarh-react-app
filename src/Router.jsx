import React, { Suspense, lazy } from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import SuspenseFallback from '@/components/system/SuspenseFallback'
import { LANDING_PAGE_PATH } from '@/constants'

const LandingPage = lazy(() => {
  return import(/* webpackChunkName: "landing" */ '@/components/pages/Landing')
})

export default () => (
  <Router>
    <Suspense fallback={<SuspenseFallback />}>
      <Switch>
        <Route
          exact
          path={LANDING_PAGE_PATH}
          component={LandingPage} />
      </Switch>
    </Suspense>
  </Router>
)

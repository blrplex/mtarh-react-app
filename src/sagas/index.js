import { fork } from 'redux-saga/effects'

function * foo () {}

// Fork common sagas only
export default function * () {
  yield fork(foo)
}

import styled from 'styled-components'

export const PaddedContent = styled.div`
  padding: 0 50px;
`

export const Footer = styled.footer`
  text-align: center;
`

import React from 'react'
import pt from 'prop-types'

import { PaddedContent, Footer } from './styles'

const BasicLayout = ({ children, hideHeader, hideFooter }) => (
  <div>
    {!hideHeader && (
      <header>
        Menu
      </header>
    )}
    <PaddedContent>
      {children}
    </PaddedContent>
    {!hideFooter && (
      <Footer>
        Put footer here
      </Footer>
    )}
  </div>
)

BasicLayout.propTypes = {
  children: pt.node.isRequired,
  hideHeader: pt.bool,
  hideFooter: pt.bool,
}

export default BasicLayout

import React, { useState } from 'react'

const Greeter = () => {
  const [focused, toggleFocus] = useState(false)
  const handleMouseEnter = () => toggleFocus(true)
  const handleMouseLeave = () => toggleFocus(false)

  const classes = [
    'greeter',
    focused ? 'focused' : 'not-focused',
  ]

  return (
    <div
      className={classes.join(' ')}
      data-testid="greeter"
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      style={{ textDecoration: focused ? 'underline' : 'none' }}
    >
      Hi, I am a stub component. Use me only if you drunk and do not have way to get back.
    </div>
  )
}

export default Greeter

/* global test, expect */

import React from 'react'
import { render, fireEvent } from '@testing-library/react'

import Greeter from './component'

test('Greeter should renders', () => {
  const component = render(<Greeter />)
  const greeterContainer = component.queryByTestId('greeter')

  // Text should be as expected
  expect(greeterContainer.textContent).toBe('Hello, world!')

  // Class names too
  expect(greeterContainer.classList.contains('not-focused')).toBeTruthy()
  expect(greeterContainer.classList.contains('focused')).toBeFalsy()

  // Entering mouse
  fireEvent.mouseEnter(greeterContainer)

  // And expecting classname change
  expect(greeterContainer.classList.contains('not-focused')).toBeFalsy()
  expect(greeterContainer.classList.contains('focused')).toBeTruthy()

  // Leaving mouse
  fireEvent.mouseLeave(greeterContainer)

  // And expecting classname restoration
  expect(greeterContainer.classList.contains('not-focused')).toBeTruthy()
  expect(greeterContainer.classList.contains('focused')).toBeFalsy()
})

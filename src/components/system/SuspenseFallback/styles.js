import styled, { keyframes } from 'styled-components'

const skRotate = keyframes`
  100% {
    transform: rotate(360deg);
  }
`

const skBounce = keyframes`
  0%, 100% { 
    transform: scale(0.0);
  } 50% { 
    transform: scale(1.0);
  }
`

export const Spinner = styled.div`
  margin: 100px auto;
  width: 40px;
  height: 40px;
  position: relative;
  text-align: center;

  animation: ${skRotate} 2.0s infinite linear;
`
export const LeftDot = styled.div`
  width: 60%;
  height: 60%;
  display: inline-block;
  position: absolute;
  top: 0;
  background-color: #333;
  border-radius: 100%;

  animation: ${skBounce} 2.0s infinite ease-in-out;
`

export const RightDot = styled.div`
  width: 60%;
  height: 60%;
  display: inline-block;
  position: absolute;
  top: 0;
  background-color: #333;
  border-radius: 100%;
  
  animation: ${skBounce} 2.0s infinite ease-in-out;

  top: auto;
  bottom: 0;
  animation-delay: -1.0s;
`

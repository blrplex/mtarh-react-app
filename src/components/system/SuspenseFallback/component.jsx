import React from 'react'

import { Spinner, LeftDot, RightDot } from './styles'

const SuspenseFallback = () => (
  <Spinner>
    <LeftDot />
    <RightDot />
  </Spinner>
)

export default SuspenseFallback

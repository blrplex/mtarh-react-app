/* global test, expect */

import React from 'react'
import { render } from '@testing-library/react'

import Component from './component'

test('Landing page should renders', () => {
  const component = render(<Component />)

  expect(component).toBeTruthy()
})

import React from 'react'

import BasicLayout from '@/components/layouts/Basic'
import Greeter from '@/components/blocks/Greeter'

export default () => (
  <BasicLayout>
    <p>Code Me</p>
    <Greeter />
  </BasicLayout>
)

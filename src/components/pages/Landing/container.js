import { connect } from 'react-redux'

import Component from './component'

const mapStateToProps = undefined

const mapDispatchToProps = undefined

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Component)

import React from 'react'
import ReactDOM from 'react-dom'

import Root from '@/Root'

ReactDOM.render(
  <Root />,
  (() => {
    const el = document.createElement('div')

    el.id = 'app-root'
    document.body.appendChild(el)

    return el
  })()
)

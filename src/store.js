import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { createLogger } from 'redux-logger'

import commonReducers from '@/reducers'
import rootSaga from '@/sagas'
import { isDevEnvironment } from '@/helpers/environment'

const isDev = isDevEnvironment()

const sagaMiddleware = createSagaMiddleware()
const middlewares = [sagaMiddleware]

if (isDev) {
  middlewares.push(createLogger())
}

export const configureStore = initialState => {
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

  const store = createStore(
    commonReducers,
    initialState,
    isDevEnvironment()
      ? composeEnhancers(applyMiddleware(...middlewares))
      : applyMiddleware(...middlewares)
  )

  sagaMiddleware.run(rootSaga)
  return store
}

import { createGlobalStyle } from 'styled-components'

import Jost from '@/assets/fonts/Jost/Jost-Regular.ttf'

export default createGlobalStyle`
  @font-face {
    font-family: 'Jost';
    src: url(${Jost}) format('truetype');
    font-weight: 300;
    font-style: normal;
  }

  html, body {
    margin: 0;
    padding: 0;
    font-family: 'Jost', sans-serif;
  }
`
